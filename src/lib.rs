#![cfg_attr(not(test), no_std)]

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum HysteresisDirection {
    Up,
    Down,
}

#[cfg_attr(test, derive(Debug))]
pub struct Hysteresis<T> {
    target: T,
    up_margin: T,
    down_margin: T,
    current_direction: HysteresisDirection,
}

impl<T> Hysteresis<T>
where
    T: num_traits::Num
        + num_traits::NumCast
        + num_traits::ToPrimitive
        + core::cmp::PartialOrd
        + Copy,
{
    /// Creates new object with desired target value and up/down margin value.
    ///
    /// # Examples
    ///
    /// ```
    /// use hysteresis::{Hysteresis, HysteresisDirection};
    /// let mut hysteresis = Hysteresis::new(50, 10, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(59);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(60);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// ```
    pub fn new(target: T, margin: T, initial_direction: HysteresisDirection) -> Self {
        Self {
            target,
            up_margin: margin,
            down_margin: margin,
            current_direction: initial_direction,
        }
    }

    /// Creates new object with desired range target values.
    ///
    /// # Examples
    ///
    /// ```
    /// use hysteresis::{Hysteresis, HysteresisDirection};
    /// let mut hysteresis = Hysteresis::new_range(50, 60, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(59);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(60);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// ```
    pub fn new_range(down_target: T, up_target: T, initial_direction: HysteresisDirection) -> Self {
        if down_target > up_target {
            panic!("Hysteresis `down_target` cannot be higher than `up_target`!");
        }
        let margin = (up_target - down_target) / T::from(2).unwrap();
        let target = down_target + margin;
        Self {
            target,
            up_margin: margin,
            down_margin: margin,
            current_direction: initial_direction,
        }
    }

    /// Creates new object with desired target value and relative to target up/down margin.
    ///
    /// # Examples
    ///
    /// ```
    /// use hysteresis::{Hysteresis, HysteresisDirection};
    /// let mut hysteresis = Hysteresis::new_relative(50, 0.10, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(54);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(55);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// ```
    pub fn new_relative<U: num_traits::Float>(
        target: T,
        margin: U,
        initial_direction: HysteresisDirection,
    ) -> Self {
        let margin = num_traits::NumCast::from(U::from(target).unwrap() * margin).unwrap();
        Self {
            target,
            up_margin: margin,
            down_margin: margin,
            current_direction: initial_direction,
        }
    }

    /// Creates new object with desired target value and separate up and down margin values.
    ///
    /// # Examples
    ///
    /// ```
    /// use hysteresis::{Hysteresis, HysteresisDirection};
    /// let mut hysteresis = Hysteresis::new_asymmetric(50, 5, 10, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(54);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(55);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// let next_direction = hysteresis.next_direction(41);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// let next_direction = hysteresis.next_direction(40);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// ```
    pub fn new_asymmetric(
        target: T,
        up_margin: T,
        down_margin: T,
        initial_direction: HysteresisDirection,
    ) -> Self {
        Self {
            target,
            up_margin,
            down_margin,
            current_direction: initial_direction,
        }
    }

    /// Creates new object with desired target value and separate up and down, relative to target value, margin values.
    ///
    /// # Examples
    ///
    /// ```
    /// use hysteresis::{Hysteresis, HysteresisDirection};
    /// let mut hysteresis = Hysteresis::new_asymmetric_relative(50, 0.10, 0.20, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(54);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(55);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// let next_direction = hysteresis.next_direction(41);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// let next_direction = hysteresis.next_direction(40);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// ```
    pub fn new_asymmetric_relative<U: num_traits::Float>(
        target: T,
        up_margin: U,
        down_margin: U,
        initial_direction: HysteresisDirection,
    ) -> Self {
        Self {
            target,
            up_margin: num_traits::NumCast::from(U::from(target).unwrap() * up_margin).unwrap(),
            down_margin: num_traits::NumCast::from(U::from(target).unwrap() * down_margin).unwrap(),
            current_direction: initial_direction,
        }
    }

    /// Returns the next direction of hysteresis from provided value.
    ///
    /// # Examples
    ///
    /// ```
    /// use hysteresis::{Hysteresis, HysteresisDirection};
    /// let mut hysteresis = Hysteresis::new(50, 10, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(40);
    /// assert_eq!(next_direction, HysteresisDirection::Up);
    /// ```
    ///
    /// ```
    /// use hysteresis::{Hysteresis, HysteresisDirection};
    /// let mut hysteresis = Hysteresis::new(50, 10, HysteresisDirection::Up);
    /// let next_direction = hysteresis.next_direction(60);
    /// assert_eq!(next_direction, HysteresisDirection::Down);
    /// ```
    pub fn next_direction(&mut self, value: T) -> HysteresisDirection {
        match value {
            _ if value <= (self.target - self.down_margin) => {
                self.current_direction = HysteresisDirection::Up;
            }
            _ if value >= (self.target + self.up_margin) => {
                self.current_direction = HysteresisDirection::Down;
            }
            _ => (),
        }
        self.current_direction.clone()
    }

    /// Returns `target` field.
    pub fn target(&self) -> T {
        self.target
    }

    /// Returns `up_margin` field.
    pub fn up_margin(&self) -> T {
        self.up_margin
    }

    /// Returns `down_margin` field.
    pub fn down_margin(&self) -> T {
        self.down_margin
    }

    /// Sets `target` field.
    pub fn set_target(&mut self, target: T) {
        self.target = target;
    }

    /// Sets `up_margin` field.
    pub fn set_up_margin(&mut self, up_margin: T) {
        self.up_margin = up_margin;
    }

    /// Sets `down_margin` field.
    pub fn set_down_margin(&mut self, down_margin: T) {
        self.down_margin = down_margin;
    }

    /// Sets both `up_margin` and `down_margin` fields.
    pub fn set_margins(&mut self, margin: T) {
        self.set_up_margin(margin);
        self.set_down_margin(margin);
    }

    /// Sets both `up_margin` and `down_margin` fields relative to target.
    pub fn set_relative_margins<U: num_traits::Float>(&mut self, relative_margins: U) {
        let margin_value =
            num_traits::NumCast::from(U::from(self.target).unwrap() * relative_margins).unwrap();
        self.set_margins(margin_value);
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        Hysteresis,
        HysteresisDirection::{Down, Up},
    };
    use rstest::{fixture, rstest};

    #[fixture]
    fn hysteresis_up() -> Hysteresis<u32> {
        Hysteresis::new(50, 10, Up)
    }

    #[fixture]
    fn hysteresis_down() -> Hysteresis<u32> {
        Hysteresis::new(50, 10, Down)
    }

    #[fixture]
    fn hysteresis_range() -> Hysteresis<u32> {
        Hysteresis::new_range(45, 55, Up)
    }

    #[fixture]
    fn hysteresis_relative() -> Hysteresis<u32> {
        Hysteresis::new_relative(50, 0.10, Up)
    }

    #[fixture]
    fn hysteresis_asymmetric() -> Hysteresis<u32> {
        Hysteresis::new_asymmetric(50, 5, 10, Up)
    }

    #[fixture]
    fn hysteresis_asymmetric_relative() -> Hysteresis<u32> {
        Hysteresis::new_asymmetric_relative(50, 0.10, 0.20, Up)
    }

    #[rstest]
    fn up(mut hysteresis_up: Hysteresis<u32>) {
        dbg!(&hysteresis_up);
        let stages = vec![
            (35, Up),
            (40, Up),
            (45, Up),
            (50, Up),
            (55, Up),
            (60, Down),
            (65, Down),
            (70, Down),
        ];
        for (measurement, expected_direction) in stages {
            let direction = hysteresis_up.next_direction(measurement);
            assert_eq!(direction, expected_direction);
        }
    }

    #[rstest]
    fn down(mut hysteresis_down: Hysteresis<u32>) {
        dbg!(&hysteresis_down);
        let stages = vec![
            (65, Down),
            (60, Down),
            (55, Down),
            (50, Down),
            (45, Down),
            (40, Up),
            (35, Up),
            (30, Up),
        ];
        for (measurement, expected_direction) in stages {
            let direction = hysteresis_down.next_direction(measurement);
            assert_eq!(direction, expected_direction);
        }
    }

    #[rstest]
    fn up_down(mut hysteresis_up: Hysteresis<u32>) {
        dbg!(&hysteresis_up);
        let stages = vec![
            (30, Up),
            (35, Up),
            (40, Up),
            (45, Up),
            (50, Up),
            (55, Up),
            (60, Down),
            (65, Down),
            (70, Down),
            (65, Down),
            (60, Down),
            (55, Down),
            (50, Down),
            (45, Down),
            (40, Up),
            (35, Up),
            (30, Up),
        ];
        for (measurement, expected_direction) in stages {
            let direction = hysteresis_up.next_direction(measurement);
            assert_eq!(direction, expected_direction);
        }
    }

    #[rstest]
    fn range(mut hysteresis_range: Hysteresis<u32>) {
        dbg!(&hysteresis_range);
        assert_eq!(hysteresis_range.target, 50);
        assert_eq!(hysteresis_range.up_margin, 5);
        assert_eq!(hysteresis_range.down_margin, 5);
        let next_direction = hysteresis_range.next_direction(54);
        assert_eq!(next_direction, Up);
        let next_direction = hysteresis_range.next_direction(55);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_range.next_direction(46);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_range.next_direction(45);
        assert_eq!(next_direction, Up);
    }

    #[rstest]
    #[should_panic]
    fn range_incorrect() {
        let _ = Hysteresis::new_range(1, 0, Up);
    }

    #[rstest]
    fn relative(mut hysteresis_relative: Hysteresis<u32>) {
        dbg!(&hysteresis_relative);
        assert_eq!(hysteresis_relative.up_margin, 5);
        assert_eq!(hysteresis_relative.down_margin, 5);
        let next_direction = hysteresis_relative.next_direction(54);
        assert_eq!(next_direction, Up);
        let next_direction = hysteresis_relative.next_direction(55);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_relative.next_direction(46);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_relative.next_direction(45);
        assert_eq!(next_direction, Up);
    }

    #[rstest]
    fn asymmetric(mut hysteresis_asymmetric: Hysteresis<u32>) {
        dbg!(&hysteresis_asymmetric);
        assert_eq!(hysteresis_asymmetric.up_margin, 5);
        assert_eq!(hysteresis_asymmetric.down_margin, 10);
        let next_direction = hysteresis_asymmetric.next_direction(54);
        assert_eq!(next_direction, Up);
        let next_direction = hysteresis_asymmetric.next_direction(55);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_asymmetric.next_direction(41);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_asymmetric.next_direction(40);
        assert_eq!(next_direction, Up);
    }

    #[rstest]
    fn asymmetric_relative(mut hysteresis_asymmetric_relative: Hysteresis<u32>) {
        dbg!(&hysteresis_asymmetric_relative);
        assert_eq!(hysteresis_asymmetric_relative.up_margin, 5);
        assert_eq!(hysteresis_asymmetric_relative.down_margin, 10);
        let next_direction = hysteresis_asymmetric_relative.next_direction(54);
        assert_eq!(next_direction, Up);
        let next_direction = hysteresis_asymmetric_relative.next_direction(55);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_asymmetric_relative.next_direction(41);
        assert_eq!(next_direction, Down);
        let next_direction = hysteresis_asymmetric_relative.next_direction(40);
        assert_eq!(next_direction, Up);
    }

    #[rstest]
    fn getters() {
        let target = 50;
        let up_margin = 5;
        let down_margin = 10;
        let hysteresis = Hysteresis::new_asymmetric(target, up_margin, down_margin, Up);
        dbg!(&hysteresis);
        assert_eq!(hysteresis.target(), target);
        assert_eq!(hysteresis.up_margin(), up_margin);
        assert_eq!(hysteresis.down_margin(), down_margin);
    }

    #[rstest]
    fn setters() {
        let target = 50;
        let margin = 10;
        let up_margin = 6;
        let down_margin = 4;
        let relative_margins = 0.1;
        let relative_margins_value = (target as f32 * relative_margins) as i32;

        let mut hysteresis = Hysteresis::new(0, 0, Up);

        hysteresis.set_target(target);
        assert_eq!(hysteresis.target(), target);

        hysteresis.set_up_margin(up_margin);
        hysteresis.set_down_margin(down_margin);
        assert_eq!(hysteresis.up_margin(), up_margin);
        assert_eq!(hysteresis.down_margin(), down_margin);

        hysteresis.set_margins(margin);
        assert_eq!(hysteresis.up_margin(), margin);
        assert_eq!(hysteresis.down_margin(), margin);

        hysteresis.set_relative_margins(relative_margins);
        assert_eq!(hysteresis.up_margin(), relative_margins_value);
        assert_eq!(hysteresis.down_margin(), relative_margins_value);
    }
}
